require "fileutils"
require "shellwords"

# Copied from: https://github.com/mattbrictson/rails-template
# Add this template directory to source_paths so that Thor actions like
# copy_file and template resolve against our source files. If this file was
# invoked remotely via HTTP, that means the files are not present locally.
# In that case, use `git clone` to download them to a local temporary dir.
def add_template_repository_to_source_path
  if __FILE__ =~ %r{\Ahttps?://}
    require "tmpdir"
    source_paths.unshift(tempdir = Dir.mktmpdir("clinia-starter-"))
    at_exit { FileUtils.remove_entry(tempdir) }
    git clone: [
      "--quiet",
      "https://bitbucket.org/clinia/clinia-starter.git",
      tempdir
    ].map(&:shellescape).join(" ")

    if (branch = __FILE__[%r{clinia-starter/(.+)/template.rb}, 1])
      Dir.chdir(tempdir) { git checkout: branch }
    end
  else
    source_paths.unshift(File.dirname(__FILE__))
  end
end

def add_gems
  gem 'webpacker', '~> 3.0'
  gem 'foreman', '~> 0.84.0'
  gem 'kirby', source: 'https://repo.fury.io/etiennecl/'
end

def set_application_name
  # Add Application Name to Config
  environment "config.application_name = Rails.application.class.parent_name"

  # Announce the user where he can change the application name in the future.
  puts "You can change application name inside: ./config/application.rb"
end

def set_locale
  environment "config.i18n.default_locale = :fr"
end

def add_webpack
  rails_command 'webpacker:install'
end

def add_kirby
  generate 'kirby:install'
end

def add_foreman
  copy_file "Procfile"
end

def add_packages
  run "yarn add uikit rails-ujs turbolinks"
end

def copy_templates
  directory "app", force: true
end

def stop_spring
  run "spring stop"
end

# Main setup
add_template_repository_to_source_path

add_gems

after_bundle do
  set_application_name
  set_locale
  stop_spring
  add_foreman
  add_webpack
  add_packages

  # Migrate
  rails_command "db:create"
  rails_command "db:migrate"

  add_kirby
  copy_templates

  git :init
  git add: "."
  git commit: %Q{ -m 'Initial commit' }
end
