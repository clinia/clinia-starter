import Rails from 'rails-ujs';
import Turbolinks from 'turbolinks';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

// Styles
import './styles/site.scss';

// Images
import './images';

Rails.start();
Turbolinks.start();

UIkit.use(Icons);
